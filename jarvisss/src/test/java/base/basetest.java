package base;

import java.io.FileReader;
import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

import io.github.bonigarcia.wdm.WebDriverManager;

public class basetest {
	public static WebDriver driver;
	public static Properties pr = new Properties();
	public static Properties loc = new Properties();
	public static FileReader fr;
	public static FileReader fr1;


   @BeforeTest
	public void setup() throws Throwable {
		if(driver==null) {
			FileReader fr = new FileReader("C:\\Users\\SSC\\eclipse-workspace\\jarvisss\\src\\test\\resources\\configure\\configure.properties");
			FileReader fr1 = new FileReader("C:\\Users\\SSC\\eclipse-workspace\\jarvisss\\src\\test\\resources\\configure\\locator.properties");
			pr.load(fr);
			loc.load(fr1);
		}
		
		if(pr.getProperty("browser").equalsIgnoreCase("chrome")) {
		WebDriverManager.chromedriver().setup(); //base
		 driver = new ChromeDriver(); //base
		 driver.get(pr.getProperty("testurl"));//properties
		 Thread.sleep(2000);
		
	}
	else if(pr.getProperty("browser").equalsIgnoreCase("firefox")) { 
	WebDriverManager.firefoxdriver().setup(); //base
	 driver = new FirefoxDriver();//base
	 driver.get(pr.getProperty("testurl")); //properties
	 Thread.sleep(2000);

}
}
	@AfterTest
	public void teardown() {
	
		

}
}

